<?php

/*
 * Copyright (C) 2016 luc && sgautorepondeur.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace SGApi;

use SGApi\SGException;

class SGApi
{

    private $membreid;
    private $codeactivation;
    private $datas         = [];
    private $apiUrl        = 'https://sg-autorepondeur.com/api/';

    public function __construct($membreid, $codeactivation) {
        $this->membreid       = $membreid;
        $this->codeactivation = $codeactivation;
        
        $this->datas['membreid']       = $this->membreid;
        $this->datas['codeactivation'] = $this->codeactivation;
    }
    /**
     * 
     * @param string $name
     * @param array $value
     * @return \sgautorepondeur\SGApi
     */
    public function set( $name, $value = null) {
        if (is_array($name)) {
            foreach ($name as $id => $value) {
                $this->set($id, $value);
            }
        } else {
            $this->datas[$name] = $value;
        }
        return $this;
    }
    
    /**
     * @name call()
     * @access public
     * @param string $action
     * @return type
     * @throws SGException
     */
    public function call($action)
    {
        $options = [
            'http' => [
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($this->datas)
            ]
        ];

        $context  = stream_context_create($options);
        $result = file_get_contents($this->apiUrl.$action, false, $context);
        if ($result === false) {
            throw new SGException('Aucun r�sultat renvoy� par SG-Autor�pondeur',500);
        } else {
            $result = json_decode($result, true);
            if (!$result['valid']) {
                $this->printError($result);
            }
        }
        return $result;
    }
    /**
     * 
     * @param type $call
     * @throws SGException
     */
    public function printError($call) {
        if (isset($call->error)) {
            throw new SGException($call->error);
        }
        if (isset($call->errors)) {
            $error = null;
            foreach ($call->errors as $errorValue) {
                $error .= $errorValue . '<br>';
            }
            throw new SGException($error);
        }
    }

}
